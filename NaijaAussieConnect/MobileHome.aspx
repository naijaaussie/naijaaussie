﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MobileHome.aspx.cs" Inherits="YAF.MobileHome" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
 
 <head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-width=1.0" />

    <title>Naija Aussie Connect</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
    <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css" />

    <script>

        $(document).ready(function () {
            $("body").show();
        });

    </script>

 </head>

 <body style="display:none">
    <form id="form1" runat="server">
    <header> 
    <div class="menu_area">     
     <nav class="navbar navbar-inverse navbar-fixed-top" style="background-color:white;color:black;border-color:white">
      <div class="container">
        <div class="navbar-header" style="width:auto">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" style="background-color:white;border-color:white; margin-top:23px">
            <span class="icon-bar" style="background-color:#81cefc; width:50px; height:10px"></span>
            <span class="icon-bar" style="background-color:#81cefc; width:50px; height:10px; margin-top:5px"></span>
            <span class="icon-bar" style="background-color:#81cefc; width:50px; height:10px; margin-top:5px"></span>
          </button>
          <a href="http://localhost:50165/default.aspx" class="navbar-brand"><img src="Images\logo3.png" style="width:450px; margin-top:2px;margin-left:25px"/></a>
        </div>
                    
        <br /><br />

        <div class="navbar-collapse collapse" style="width:auto; max-height:1000px; margin-top:42px">
          <ul class="nav navbar-nav navbar-right" style="height:500px">
            <li><br /><a runat="server" href="/forum" style="color:black;font-size:40pt">Forum</a><br /><br /><br /></li>
            <li><a runat="server" href="/mytopics" style="color:black;font-size:40pt">Active Topics</a><br /><br /><br /></li>
            <li><a runat="server" href="/search" style="color:black;font-size:40pt">Search</a><br /><br /><br /></li>
            <li><a runat="server" href="/help_index" style="color:black;font-size:40pt">Help</a><br /><br /><br /></li>
            <li><a runat="server" href="/login" style="color:black;font-size:40pt">Login</a><br /><br /><br /></li>
            <li><a runat="server" href="/rules" style="color:black;font-size:40pt">Register</a><br /><br /><br /></li> 
          </ul>
        </div>
      
      </div>

     </nav>
    </div>
   </header>        
 
   <br /><br />

   <div style="background:url('Images//Home/MobileFinal.jpg') no-repeat; width:1200px; height:auto; background-size:cover">
    
    <img src="Images\Home\clip1.png" style="width:600px; margin-left:318px; margin-bottom:10px;margin-top:-100px"/> 
     
    <iframe id="video" class="video" src="https://www.youtube.com/embed/yVc4MungHjs" style="width: 822px; height: 424px; margin-left: 199px; margin-bottom: 497px; margin-top: -17px; border:none"></iframe>    
     
    <a href="#" id="stopbtn" class="btn btn-primary" style="margin-left:-66px; margin-top:-1772px; width:50px;height:55px;"><span class="glyphicon glyphicon-remove" style="margin-left:-8px;margin-bottom:12px;font-size:2.8em;vertical-align: middle"></span></a>   
           
    <a href="#"><img src="Images\Home\b06.png" style="width:245px;margin-left:492px;margin-bottom:10px;margin-top:-716px"/></a>
    
    <br />

    <a href="#"><img src="Images\Home\b01.png" style="width:300px;height:80px;margin-left:606px;margin-top:-406px"/></a>

    <br />

    <a href="#"><img src="Images\Home\b2.png" style="width:300px;height:80px;margin-left:297px;margin-top:-300px"/></a>
    
    <br />

    <a href="#"><img src="Images\Home\b3.png" style="width:300px;height:80px;margin-left:606px;margin-top:-185px"/></a>

    <br />

    <a href="#"><img src="Images\Home\b4.png" style="width:300px;height:80px;margin-left:297px;margin-top:-72px"/></a>
    
    <br />
       
    <a href="#"><img src="Images\Home\b5.png" style="width:300px;height:80px;margin-left:606px;margin-top:3px"/></a>

    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
   
   </div>
  </form>
 
 </body>

</html>

<script type="text/javascript">
      
       $("#stopbtn").click(function () {
           $("#video")[0].src += "?";
       });

</script>
