﻿<%@ Page Language="C#" AutoEventWireup="true" ValidateRequest="false" Inherits="YAF.ForumPageBase" %>
<%@ Register TagPrefix="YAF" Assembly="YAF" Namespace="YAF" %>
<%@ Register TagPrefix="url" Namespace="Intelligencia.UrlRewriter" Assembly="Intelligencia.UrlRewriter" %>

<script runat="server">
    
    

</script>

<!doctype html>
  <html lang="en">

   <head id="YafHead" runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
    <meta http-equiv="x-ua-compatible" content="ie=edge"><meta id="YafMetaScriptingLanguage" http-equiv="Content-Script-Type" runat="server" name="scriptlanguage" content="text/javascript" />
    <meta id="YafMetaStyles" http-equiv="Content-Style-Type" runat="server" name="styles" content="text/css" />
    <meta id="YafMetaDescription" runat="server" name="description" content="Naija Aussie Connect -- A bulletin board system written in ASP.NET" />
    <meta id="YafMetaKeywords" runat="server" name="keywords" content="Naija Aussie Connect, Forum, ASP.NET, BB, Bulletin Board, opensource" />
          
    <title></title>
  
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
    <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css" />   
         
    <script>

        if (window.location == 'http://localhost:50165/')
        {
           window.location = 'http://localhost:50165/default.aspx';
        }

    </script>

   </head>
    
   <body id="YafBody" runat="server" style="margin: 0; padding: 0px; background-color:#f2f2f2">
                  
    <url:Form id="form1" runat="server" enctype="multipart/form-data">
      
     <YAF:Forum runat="server" ID="forum">

     </YAF:Forum> 
         
    </url:Form>                 

   </body>

</html>