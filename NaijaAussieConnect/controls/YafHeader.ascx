﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="YafHeader.ascx.cs" Inherits="YAF.Controls.YafHeader" CodeFile="~/controls/YafHeader.ascx.cs" %>

<!DOCTYPE html>

 <html xmlns="http://www.w3.org/1999/xhtml">
  
  <head>   
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    
    <title>Home</title>

    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
    <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css" />
    
    
    <style type="text/css">
       @font-face
       {
         font-family: myFirstFont;  
       }

       a
       {
         font-family: myFirstFont;
       }
    </style>

  </head>

  <body style="background-color:#81cefc">

  <div class="menu_area">         
   <nav id="yafheader" class="navbar navbar-inverse" style="background-color:white;border-color:white">
    <!--<asp:Panel id="GuestUserMessage" CssClass="guestUser" runat="server" Visible="false">
      <asp:Label id="GuestMessage" runat="server"></asp:Label>
    </asp:Panel>-->
   
    <div class="container-fluid">
     
       <div class="navbar-header" style="width:auto">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" style="background-color:white;border-color:white;text-align:center;height:2px;margin-top:14px;margin-right:1px">
            <span class="icon-bar" style="background-color:#f2f2f2; height:4px"></span>
            <span class="icon-bar" style="background-color:#f2f2f2; height:4px"></span>
            <span class="icon-bar" style="background-color:#f2f2f2; height:4px"></span>
        </button>     
      <a href="http://localhost:50165/default.aspx"><img class="nav navbar-left" src="http://localhost:50165/Images/logo3.png" style="width:200px"/></a> 
     </div>
                  
    <div class="collapse navbar-collapse" style="width:auto"> 

      <div class="menuContainer nav navbar-nav navbar-right" style="font-size:12pt;margin-top:23px;margin-right:-2px">           
         
         <asp:Panel id="LoggedInUserPanel" CssClass="loggedInUser" runat="server" Visible="false">
         </asp:Panel> 
                     
         <asp:Panel id="UserContainer" CssClass="menuMyContainer" runat="server" Visible="false" Font-Size="12pt">
            <ul class="menuMyList">
                             
                <li class="menuMy myProfile">
                  <asp:HyperLink id="MyProfile" runat="server" Target="_top"></asp:HyperLink>
                </li>
                
                <asp:HyperLink ID="MyInboxItem" runat="server">
                </asp:HyperLink>
                <asp:PlaceHolder ID="MyBuddiesItem" runat="server">
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="MyAlbumsItem" runat="server">
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="MyTopicItem" runat="server">
                </asp:PlaceHolder>
            </ul>
             
        </asp:Panel>
                          
        <div class="menuContainer nav navbar-nav navbar-right" style="font-size:12pt; margin-top:1px; margin-left:-6px">
            <ul class="menuList">
             <li>
              <asp:PlaceHolder ID="menuListItems" runat="server">
              </asp:PlaceHolder>
             </li>
            </ul>
                        
            <asp:Panel ID="quickSearch" runat="server" CssClass="QuickSearch" Visible="false">
               <asp:TextBox ID="searchInput" runat="server"></asp:TextBox>
               <asp:LinkButton ID="doQuickSearch" onkeydown="" runat="server" CssClass="QuickSearchButton" OnClick="QuickSearchClick">
               </asp:LinkButton>
            </asp:Panel>
            
            <asp:PlaceHolder ID="LogutItem" runat="server" Visible="false">
               <ul>   
                <li class="menuAccount">
                   <asp:LinkButton ID="LogOutButton" runat="server" OnClick="LogOutClick" OnClientClick="createCookie('ScrollPosition',document.all ? document.scrollTop : window.pageYOffset);"></asp:LinkButton>
                </li>
               </ul>
            </asp:PlaceHolder>
            
            <asp:PlaceHolder ID="AdminModHolder" runat="server" Visible="false">
              <ul class="menuAdminList">
                <asp:PlaceHolder ID="menuAdminItems" runat="server"></asp:PlaceHolder>
              </ul>
            </asp:PlaceHolder>
        </div>
       </div>
      </div>
    </div>
    </nav>
   </div>  
   <br /><br /> 

 </body>

</html>