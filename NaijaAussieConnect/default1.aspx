﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default1.aspx.cs" Inherits="YAF.Home" %>

<!doctype html>

 <html xmlns="http://www.w3.org/1999/xhtml">

 <head runat="server">
   <meta charset="utf-8" />
   <meta name="viewport" content="width=device-width, initial-scale=1" />
   <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" /> 
   
   <title>Naija Aussie Connect - Home</title>    
   
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
   <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css" />    
   
   <style type="text/css">
       @font-face
       {
         font-family: myFirstFont;
         src: url(Content/fonts/glyphicons-halflings-regular.ttf);
         font-size:14pt;  
       }

       a
       {
         font-family: myFirstFont;
       }
    </style>    
    
    <script>
        
        if(screen.width <= 500 && screen.width <= 800)
        {
            window.location = 'http://localhost:50165/MobileHome.aspx';
        }

        else if (screen.width < 1300)
        {
            window.location = 'http://localhost:50165/TabletHome.aspx';
        }

        $(document).ready(function () {
            $("body").show();
        });

    </script>

 </head>

 <body style="display:none">
   
  <form id="form1" runat="server">
   
   <header> 
    <div class="menu_area">     
     <nav class="navbar navbar-inverse navbar-fixed-top" style="background-color:white;color:black;border-color:white">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" style="background-color:#81cefc;border-color:white">
            <span class="icon-bar" style="background-color:white"></span>
            <span class="icon-bar" style="background-color:white"></span>
            <span class="icon-bar" style="background-color:white"></span>
          </button>
          <a href="http://localhost:50165/default.aspx" class="navbar-brand"><img src="Images\logo3.png"/></a>
        </div>
        
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a runat="server" href="/forum" style="color:black;font-size:12pt"><b>Forum</b></a></li>
            <li><a runat="server" href="/mytopics" style="color:black;font-size:12pt"><b>ActiveTopics</b></a></li>
            <li><a runat="server" href="/search" style="color:black;font-size:12pt"><b>Search</b></a></li>
            <li><a runat="server" href="/help_index" style="color:black;font-size:12pt"><b>Help</b></a></li>
            <li><a runat="server" href="/login" style="color:black;font-size:12pt"><b>Login</b></a></li>
            <li><a runat="server" href="/rules" style="color:black;font-size:12pt"><b>Register</b></a></li> 
          </ul>
        </div>
      
      </div>
     </nav>
    </div>
   </header>
     
   <br /><br />
    
     <div style="background:url('Images/Home/FinalHome.jpg') no-repeat; padding-top: 6px; height: 620px">
       
       <img src="Images\Home\clip1.png" style="margin-left:651px; margin-bottom:10px;margin-top:-87px"/>       
     
        <a href="#"><img src="Images\Home\b06.png" style="margin-left:1018px;margin-bottom:189px;margin-top:-311px"/></a>                                   
                   
        <a href="#"><img src="Images\Home\b01.png" style="margin-left:210px;margin-bottom:186px;margin-top:-220px"/></a> 
     
        <br />
              
        <a href="#"><img src="Images\Home\b2.png" style="margin-left:16px;margin-bottom:161px;margin-top:-192px"/></a>
     
        <br />
                         
        <a href="#"><img src="Images\Home\b3.png" style="margin-left:211px;margin-bottom:132px;margin-top:-172px"/></a>

        <br />
                     
        <a href="#"><img src="Images\Home\b4.png" style="margin-left:36px;margin-bottom:104px;margin-top:-150px"/></a> 
     
        <br />
     
        <a href="#"><img src="Images\Home\b5.png" style="margin-left:211px;margin-bottom:-107px;margin-top:-390px"/></a>   
                                   
        <iframe id="video" class="video" src="https://www.youtube.com/embed/yVc4MungHjs" style="width: 528px; height: 275px; margin-left: 165px; margin-bottom: -79px; margin-top: -138px; border:none"></iframe> 
          
        <a id="stopbtn" class="btn btn-primary" style="margin-left:-35px; margin-top:-350px; width:5px;height:29px;"><span class="glyphicon glyphicon-remove" style="margin-left:-8px;margin-bottom:12px;font-size:1.2em;vertical-align: middle"></span></a>
         
      </div>
    </form>

</body>
</html>

<script type="text/javascript">
      
       $("#stopbtn").click(function () {
           $("#video")[0].src += "?";
       });

</script>
