﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="YafHeader.ascx.cs" Inherits="YAF.Controls.YafHeader" %>

<!DOCTYPE html>

 <html xmlns="http://www.w3.org/1999/xhtml">
  
  <head>  
    <!--<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Home</title>
    
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />-->
    
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Home</title>

    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://www.w3schools.com/lib/w3.css" />
    
    
    <style type="text/css">
       @font-face
       {
         font-family: myFirstFont;  
       }

       a
       {
         font-family: myFirstFont;
       }
    </style>
 
  </head>

  <body style="background-color:#81cefc">
         
   <nav id="yafheader" class="navbar navbar-inverse" style="background-color:white;border-color:white">
    <!--<asp:Panel id="GuestUserMessage" CssClass="guestUser" runat="server" Visible="false">
      <asp:Label id="GuestMessage" runat="server"></asp:Label>
    </asp:Panel>-->
   
   <div class="container">
    
    <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a href="#" class="navbar-brand"><img src="Images\logo3.png"/></a>
    </div>
    
    <br />   
               
    <div class="collapse navbar-collapse" style="text-align:center">
     
      <div class="menuContainer nav navbar-nav navbar-right" style="font-size:10pt">           
        
         <asp:Panel id="UserContainer" CssClass="menuMyContainer" runat="server" Visible="false" Font-Size="10pt">
            <ul class="menuMyList">
                
                <li class="menuMy myProfile">
                  <asp:HyperLink id="MyProfile" runat="server" Target="_top"></asp:HyperLink>
                </li>
                
                <asp:HyperLink ID="MyInboxItem" runat="server">
                </asp:HyperLink>
                <asp:PlaceHolder ID="MyBuddiesItem" runat="server">
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="MyAlbumsItem" runat="server">
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="MyTopicItem" runat="server">
                </asp:PlaceHolder>
            </ul>
        </asp:Panel>
        
        <asp:Panel id="LoggedInUserPanel" CssClass="loggedInUser" runat="server" Visible="false">
        </asp:Panel>
        
        <div class="menuContainer nav navbar-nav navbar-right" style="font-size:10pt">
            <ul class="menuList">
                <asp:PlaceHolder ID="menuListItems" runat="server">
                </asp:PlaceHolder>
            </ul>
            <asp:Panel ID="quickSearch" runat="server" CssClass="QuickSearch" Visible="false" >
               <asp:TextBox ID="searchInput" runat="server"></asp:TextBox>&nbsp;
               <asp:LinkButton ID="doQuickSearch" onkeydown="" runat="server" CssClass="QuickSearchButton" OnClick="QuickSearchClick">
               </asp:LinkButton>
            </asp:Panel>

            <asp:PlaceHolder ID="LogutItem" runat="server" Visible="false">
              <ul>   
                <li class="menuAccount">
                   <asp:LinkButton ID="LogOutButton" runat="server" OnClick="LogOutClick" OnClientClick="createCookie('ScrollPosition',document.all ? document.scrollTop : window.pageYOffset);"></asp:LinkButton>
                 </li>
             </ul>
            </asp:PlaceHolder>

            <asp:PlaceHolder ID="AdminModHolder" runat="server" Visible="false">
              <ul class="menuAdminList">
                <asp:PlaceHolder ID="menuAdminItems" runat="server"></asp:PlaceHolder>
              </ul>
            </asp:PlaceHolder>
        </div>
       </div>
      </div>
     </div>
    </nav>
    
   <br /><br />  

 </body>

</html>