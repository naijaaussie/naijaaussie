﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TabletHome.aspx.cs" Inherits="YAF.TabletHome" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-width=1.0" />

    <title>Naija Aussie Connect</title>

    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
    <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css" />
    
    <script>

        $(document).ready(function () {
            $("body").show();
        });

    </script> 

</head>

<body style="display:none">
  <form id="form1" runat="server">
   <header> 
    <div class="menu_area">     
     <nav class="navbar navbar-inverse navbar-fixed-top" style="background-color:white;color:black;border-color:white">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" style="background-color:white;border-color:white">
            <span class="icon-bar" style="background-color:#81cefc; width:15px"></span>
            <span class="icon-bar" style="background-color:#81cefc; width:15px"></span>
            <span class="icon-bar" style="background-color:#81cefc; width:15px"></span>
          </button>
          <a href="http://naijaaussie.tk/default.aspx" class="navbar-brand"><img src="Images\logo3.png" style="width:300px; margin-top:11px;margin-left:-106px"/></a>
        </div>

        <div class="navbar-collapse collapse" style="margin-right:-117px">
          <ul class="nav navbar-nav navbar-right">
            <li><a runat="server" href="/forum" style="color:black;font-size:14pt">Forum</a></li>
            <li><a runat="server" href="/mytopics" style="color:black;font-size:14pt">Active Topics</a></li>
            <li><a runat="server" href="/search" style="color:black;font-size:14pt">Search</a></li>
            <li><a runat="server" href="/help_index" style="color:black;font-size:14pt">Help</a></li>
            <li><a runat="server" href="/login" style="color:black;font-size:14pt">Login</a></li>
            <li><a runat="server" href="/rules" style="color:black;font-size:14pt">Register</a></li> 
          </ul>
        </div>
      
      </div>
     </nav>
    </div>
   </header>        
 
   <div style="background:url('Images/Home/MobileFinal.jpg') no-repeat; width:1400px; height:auto; background-size:cover">
    
    <img src="Images\Home\clip1.png" style="width:600px; margin-left:424px; margin-bottom:10px;margin-top:-44px"/>
    
    <iframe id="video" class="video" src="https://www.youtube.com/embed/yVc4MungHjs" style="width: 958px; height: 507px; margin-left: 232px; margin-bottom: 497px; margin-top: -21px; border:none"></iframe>    

    <a href="#" id="stopbtn" class="btn btn-primary" style="margin-left:-67px; margin-top:-1930px; width:50px;height:55px;"><span class="glyphicon glyphicon-remove" style="margin-left:-8px;margin-bottom:12px;font-size:2.8em;vertical-align: middle"></span></a>   

    <a href="#"><img src="Images\Home\b06.png" style="width:250px;margin-left:596px;margin-bottom:10px;margin-top:-666px"/></a>
    
    <br />

    <a href="#"><img src="Images\Home\b01.png" style="width:450px;height:100px;margin-left:706px;margin-bottom:10px;margin-top:-242px"/></a>

    <br />

    <a href="#"><img src="Images\Home\b2.png" style="width:450px;height:100px;margin-left:245px;margin-bottom:10px;margin-top:-92px"/></a>
    
    <br />

    <a href="#"><img src="Images\Home\b3.png" style="width:450px;height:100px;margin-left:704px;margin-bottom:10px;margin-top:-10px"/></a>

    <br />

    <a href="#"><img src="Images\Home\b4.png" style="width:450px;height:100px;margin-left:246px;margin-bottom:10px;margin-top:-13px"/></a>
    
    <br />

    <a href="#"><img src="Images\Home\b5.png" style="width:450px;height:100px;margin-left:704px;margin-bottom:10px;margin-top:-11px"/></a>

    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
   
    </div>

 </form>
</body>
</html>

<script type="text/javascript">
      
       $("#stopbtn").click(function () {
           $("#video")[0].src += "?";
       });

</script>